#pragma once
#ifndef BMP_HEADER_H
#define BMP_HEADER_H

#define BMP_SIGNATURE 0x4d42
#define BMP_PIXEL_DATA_OFFSET 54
#define BMP_HEADER_SIZE 40
#define BMP_PLANES 1
#define BMP_BITS_PER_PIXEL 24
#define BMP_COMPRESSION 0

#include <stdint.h>
#include <stdio.h>

#include "image.h"

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType; //BMP_SIGNATURE
        uint32_t bfileSize;
        uint32_t bfReserved; //0
        uint32_t bOffBits; //BMP_PIXEL_DATA_OFFSET
        uint32_t biSize; //BMP_HEADER_SIZE
        uint32_t biWidth; 
        uint32_t biHeight;
        uint16_t biPlanes; //BMP_PLANES
        uint16_t biBitCount; //BMP_BITS_PER_PIXEL
        uint32_t biCompression; //BMP_COMPRESSION
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter; //0
        uint32_t biYPelsPerMeter;//0
        uint32_t biClrUsed; //0
        uint32_t biClrImportant; //0
};
#pragma pack(pop)

enum bmp_read_errors{
        BMP_READ_OK = 0,
        BMP_READ_ERROR_FREAD,
        BMP_READ_ERROR_IO,
        BMP_READ_ERROR_INVALID_SIGNATURE,
        BMP_READ_ERROR_INVALID_BITS,
        BMP_READ_ERROR_INVALID_PADDING,
        BMP_READ_ERROR_UNSUPPORTED,
        BMP_READ_ERROR_UNEXPECTED_EOF
};

enum bmp_write_errors{
        BMP_WRITE_OK = 0,
        BMP_WRITE_ERROR_FWRITE,
        BMP_WRITE_ERROR_IO,
        BMP_WRITE_ERROR_INVALID_HEADER,
        BMP_WRITE_ERROR_INVALID_PADDING,
        BMP_WRITE_ERROR_UNEXPECTED_EOF
};

struct bmp_header create_bmp_header(const struct image* image);

enum bmp_read_errors bmp_header_read_validate(struct bmp_header* header);
enum bmp_write_errors bmp_header_write_validate(struct bmp_header* header);

enum bmp_read_errors from_file_read_image(FILE* path, struct image* image);
enum bmp_write_errors write_image_to_file(FILE* path, struct image* image);

enum bmp_read_errors bmp_header_read(FILE* filename, struct bmp_header* header);
enum bmp_write_errors bmp_header_write(FILE* filename, struct bmp_header* header);

uint64_t image_size(uint64_t width, uint64_t height);

void print_bmp_read_error_message(enum bmp_read_errors status);
void print_bmp_write_error_message(enum bmp_write_errors status);

#endif
