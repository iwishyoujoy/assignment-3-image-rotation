#pragma once
#ifndef FILE_ACTION_H
#define FILE_ACTION_H

#include <stdio.h>

#include "bmp_header.h"
#include "image.h"

enum bmp_read_errors read_image(const char* filename, const char* mode, struct image* image);
enum bmp_write_errors write_image(const char* filename, const char* mode, struct image* image);

#endif
