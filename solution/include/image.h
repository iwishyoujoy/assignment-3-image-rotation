#pragma once
#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>

#pragma pack(push, 1)
struct pixel{
        uint8_t b;
        uint8_t g;
        uint8_t r;
};
#pragma pack(pop)

struct image {
        uint64_t width; 
        uint64_t height;
        struct pixel* data;
};

struct image create_image(uint64_t width, uint64_t height);
void free_image(struct image* image);

struct pixel image_get_pixel(struct image const* image, uint64_t x, uint64_t y);
void image_set_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel pixel);

uint64_t padding_get(uint64_t width);

#endif
