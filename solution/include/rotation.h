#pragma once
#ifndef ROTATION_H
#define ROTATION_H

#include "image.h"

struct image rotate_image(struct image const* cur_image);

#endif

