#include <errno.h>
#include <string.h>

#include "bmp_header.h"
#include "image.h"

struct bmp_header create_bmp_header(const struct image* image){
    return (struct bmp_header){
        .bfType = BMP_SIGNATURE,
        .bfileSize = sizeof(struct bmp_header) + (image->width * sizeof(struct pixel) + padding_get(image->width)) * image->height,
        .bfReserved = 0,
        .bOffBits = BMP_PIXEL_DATA_OFFSET,
        .biSize = BMP_HEADER_SIZE,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = BMP_PLANES,
        .biBitCount = BMP_BITS_PER_PIXEL,
        .biCompression = BMP_COMPRESSION,
        .biSizeImage = image_size(image->width, image->height),
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

uint64_t image_size(uint64_t width, uint64_t height){
    return sizeof(struct pixel)*height*width+padding_get(width);
}

enum bmp_read_errors bmp_header_read_validate(struct bmp_header* header){
    if (header->bfType != BMP_SIGNATURE){
        return BMP_READ_ERROR_INVALID_SIGNATURE;
    }
    if ((header->biPlanes != BMP_PLANES) || (header->biBitCount != BMP_BITS_PER_PIXEL) || (header->biCompression != BMP_COMPRESSION)){
        return BMP_READ_ERROR_UNSUPPORTED;
    }
    return BMP_READ_OK;
}

enum bmp_read_errors bmp_header_read(FILE* filename, struct bmp_header* header){
    size_t read_count = fread(header, sizeof(struct bmp_header), 1, filename);
    if (read_count == 1) {
        return bmp_header_read_validate(header);
    }
    return BMP_READ_ERROR_FREAD;
}

enum bmp_read_errors from_file_read_image(FILE* path, struct image* image){
    struct bmp_header header;

    enum bmp_read_errors status = bmp_header_read(path, &header);
    if (status != 0){
        return status;
    }

    if(fseek(path, header.bOffBits, SEEK_SET)!=0){
        return BMP_READ_ERROR_IO;
    }

    uint64_t padding = padding_get(header.biWidth);

    *image = create_image(header.biWidth, header.biHeight);
    if (image->data == NULL){
        return BMP_READ_ERROR_IO;
    }

    for(size_t i = 0; i < header.biHeight; i++){
        if (fread((image)->data + i * header.biWidth, sizeof(struct pixel) * header.biWidth, 1, path) != 1){
            free_image(image);
            return BMP_READ_ERROR_UNEXPECTED_EOF;
        }   
        if (fseek(path, (long)padding, SEEK_CUR) != 0){
            free_image(image);
            return BMP_READ_ERROR_INVALID_PADDING;
        };
    }
    return BMP_READ_OK;
}

enum bmp_write_errors bmp_header_write(FILE* filename, struct bmp_header* header){
    size_t write_count = fwrite(header, sizeof(struct bmp_header), 1, filename);
    if (write_count == 1){
        return BMP_WRITE_OK;
    }
    return BMP_WRITE_ERROR_FWRITE;
}

enum bmp_write_errors write_image_to_file(FILE* path, struct image* image){
    struct bmp_header header = create_bmp_header(image);
    uint64_t padding = padding_get(header.biWidth);

    if (bmp_header_write(path, &header) != BMP_WRITE_OK){
        return BMP_WRITE_ERROR_INVALID_HEADER;
    }
    if (fseek(path, header.bOffBits, SEEK_SET) != 0){
        return BMP_WRITE_ERROR_INVALID_HEADER;
    }

    for(size_t i = 0; i < header.biHeight; i++){
        if (fwrite(image->data + i * header.biWidth, sizeof(struct pixel) * header.biWidth, 1, path) != 1){
            printf("%s\n", strerror(errno));
            return BMP_WRITE_ERROR_UNEXPECTED_EOF;
        }           

        char padding_bytes[4] = {0};
        if (fwrite(padding_bytes, padding, 1, path) != 1){
            return BMP_WRITE_ERROR_INVALID_PADDING;
        }            
    } 
    return BMP_WRITE_OK;
}

