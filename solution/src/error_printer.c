#include <errno.h>
#include <string.h>

#include "bmp_header.h"

char* bmp_read_error_message(enum bmp_read_errors status){
    switch(status){
        case BMP_READ_OK:
            return "BMP was read successfully.";
        case BMP_READ_ERROR_FREAD:
            return "Something gone wrong with fread() function.";
        case BMP_READ_ERROR_IO:
            return strerror(errno);
        case BMP_READ_ERROR_INVALID_SIGNATURE:
            return "BMP read error: Invalid BMP signature.";
        case BMP_READ_ERROR_INVALID_BITS:
            return "BMP read error: Invalid BMP bits.";
        case BMP_READ_ERROR_INVALID_PADDING:
            return "BMP read error: Invalid BMP padding.";
        case BMP_READ_ERROR_UNSUPPORTED:
            return "BMP read error: Unsupported BMP.";
        case BMP_READ_ERROR_UNEXPECTED_EOF:
            return "BMP read error: Unexpected end of file.";
        default:
            return "BMP read error: Unknown error.";
    }
}

char* bmp_write_error_message(enum bmp_write_errors status){
    switch(status){
        case BMP_WRITE_OK:
            return "BMP was written successfully.";
        case BMP_WRITE_ERROR_FWRITE:
            return "Something gone wrong in fwrite() function.";
        case BMP_WRITE_ERROR_IO:
            return strerror(errno);
        case BMP_WRITE_ERROR_INVALID_HEADER:
            return "BMP write error: Invalid BMP header.";
        case BMP_WRITE_ERROR_INVALID_PADDING:
            return "BMP write error: Invalid BMP padding.";
        case BMP_WRITE_ERROR_UNEXPECTED_EOF:
            return "BMP write error: Unexpected end of file.";
        default:
            return "BMP write error: Unknown error.";
    }
}

void print_bmp_read_error_message(enum bmp_read_errors status){
    fprintf(stderr, "%s\n", bmp_read_error_message(status));
}

void print_bmp_write_error_message(enum bmp_write_errors status){
    fprintf(stderr, "%s\n", bmp_write_error_message(status));
}
