#include "bmp_header.h"
#include "file_actions.h"

enum bmp_read_errors read_image(const char* filename, const char* mode, struct image* image){
        FILE* file = fopen(filename, mode);
        if (file == NULL){
                return BMP_READ_ERROR_IO;
        }
        enum bmp_read_errors status = from_file_read_image(file, image);
        fclose(file);
        return status;
}

enum bmp_write_errors write_image(const char* filename, const char* mode, struct image* image){
        FILE* file = fopen(filename, mode);
        if (file == NULL){
                return BMP_WRITE_ERROR_IO;
        }
        enum bmp_write_errors status = write_image_to_file(file, image);
        fclose(file);
        return status;
}
