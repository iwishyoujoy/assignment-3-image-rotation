#include <stdio.h>
#include <stdlib.h>

#include "image.h"

struct image create_image(uint64_t width, uint64_t height){
    struct pixel* data = calloc(height * width, sizeof(struct pixel)); 
    return (struct image){.width = width, .height = height, .data = data};
}

void free_image(struct image* image){
    free(image->data);
    image->data = NULL;
}

struct pixel image_get_pixel(struct image const* image, uint64_t x, uint64_t y){
    return image->data[image->width * y + x];
}

void image_set_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel pixel){
    image->data[image->width * y + x]=pixel;
}

uint64_t padding_get(uint64_t width){
    return width % 4 == 0 ? 0 : 4 - (width * 3) % 4;
}
