#include <stdio.h>
#include <stdlib.h>

#include "bmp_header.h"
#include "main.h"

int main( int argc, char** argv ) {
    

    if (argc < 3) {
        fprintf(stderr, "Usage: %s <in> <out> \n", argv[0]);
        return EXIT_FAILURE;
    }
    
    enum bmp_read_errors read_status;
    enum bmp_write_errors write_status;

    struct image source_image;

    read_status = read_image(argv[1], "rb", &source_image);
    if (read_status != BMP_READ_OK){
        print_bmp_read_error_message(read_status);
        free_image(&source_image);
        return EXIT_FAILURE;
    }

    struct image result_image = rotate_image(&source_image);
    write_status = write_image(argv[2], "wb", &result_image);

    free_image(&source_image);
    free_image(&result_image);
    
    if (write_status != BMP_WRITE_OK){
        print_bmp_write_error_message(write_status);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
