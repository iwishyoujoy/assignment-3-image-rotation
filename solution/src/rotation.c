#include "rotation.h"
#include <assert.h>
#include <stdio.h>

struct image rotate_image(struct image const* cur_image){
    assert(cur_image->data != NULL);
    struct image result_image = create_image(cur_image->height, cur_image->width);
    
    for (uint64_t x = 0; x < cur_image->width; x++) {
        for (uint64_t y = 0; y < cur_image->height; y++) {
            image_set_pixel(&result_image, cur_image->height - y - 1, x, image_get_pixel(cur_image, x, y));
        }
    }
    return result_image;
}
